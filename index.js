import express from "express";
import path from "path";
import bodyParser from "body-parser";
import ejsLayouts from "express-ejs-layouts";
const server = express();
const port = 8000;

server.set("view engine", "ejs");
server.set("views", path.join(path.resolve(), "src", "views"));

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(express.static("./src/views"));
server.use(ejsLayouts);

server.get("/", (req, res) => {
  res.render("home");
});
server.get("/about", (req, res) => {
  res.render("about");
});

server.listen(port, (err) => {
  if (err) console.log("Error listening to server: " + err);
  else console.log(`SERVER listening on port ${port} `);
});
